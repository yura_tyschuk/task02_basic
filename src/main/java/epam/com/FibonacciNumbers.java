/**
 * <p>
 * This package include two classes.
 * Imported Vector collections.
 *  <p>
 */
package epam.com;
import java.util.Vector;

/**
 * Fibonacci Number Class calculate fibonacci sequence,
 * print this sequence and find biggest odd and even number.
 * @author Yura Tyschuk
 * @version 1.0
 * @since 07.11.2019
 */

public final class FibonacciNumbers {

    private final int sizeOfSet;
    private Vector<Integer> storageForFibonacciNumbers;
    private static int PERCENTAGE = 100;

    /**
     * @param sizeOfSet count of numbers in sequence
     */
    public FibonacciNumbers(final int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
        storageForFibonacciNumbers = new Vector<Integer>();
    }

    public int getSizeOfSet() {
        return sizeOfSet;
    }

    public Vector<Integer> getStorageForFibonacciNumbers() {
        return storageForFibonacciNumbers;
    }


    /**
     * Calculate fibonacci numbers in a given interval
     * and add numbers to Vector.
     *
     */
    public void calculateFibonacciNumbers() {
        int firstNumberOfFibonacciSequence = 0;
        int secondNumberOfFibonacciSequence = 1;
        int resultedNumberOfFibonacciSequence;
        for (int i = 0; i < sizeOfSet; i++) {
            resultedNumberOfFibonacciSequence = firstNumberOfFibonacciSequence
                + secondNumberOfFibonacciSequence;

            storageForFibonacciNumbers.add(resultedNumberOfFibonacciSequence);

            firstNumberOfFibonacciSequence = secondNumberOfFibonacciSequence;
            secondNumberOfFibonacciSequence = resultedNumberOfFibonacciSequence;
        }
    }

    /**
     * This function prints sequence in a given interval.
     */
    public void printFibonacciNumbers() {
        System.out.println(storageForFibonacciNumbers);
    }

    /**
     * This function get percentage of even numbers in
     * calculated Fibonacci sequence.
     * @return float the percentage of even numbers
     */
    public  float getPercentageOfEvenNumbers() {
        int countOfEvenNumbers = 0;
        for (int i = 0; i < storageForFibonacciNumbers.size(); i++) {
            if (storageForFibonacciNumbers.get(i) % 2 == 0) {
                countOfEvenNumbers++;
            }
        }
        return (PERCENTAGE * countOfEvenNumbers)
                    / (float) storageForFibonacciNumbers.size();
    }


    /**
     * This function get percentage of odd numbers
     * by calculating percentage of even numbers and
     * subtract it from PERCENTAGE.
     * @return float percentage of odd numbers
     */
    public float getPercentageOfOddNumbers() {
        return PERCENTAGE - getPercentageOfEvenNumbers();
    }

    /**
     * This function get biggest odd number.
     * @return int biggest odd number
     */
    public int getBiggestOddNumber() {
        int biggestOddNumber = 0;

        for (int i = storageForFibonacciNumbers.size() - 1; i > 0; i--) {
            if (storageForFibonacciNumbers.get(i) % 2 == 1) {
                biggestOddNumber = storageForFibonacciNumbers.get(i);
                break;
            }
        }
        return biggestOddNumber;
    }

    /**
     * This function find biggest even number.
     * @return int biggest even number
     */
    public int getBiggestEvenNumber() {
        int biggestEvenNumber = 0;

        for (int i = storageForFibonacciNumbers.size() - 1; i > 0; i--) {
            if (storageForFibonacciNumbers.get(i) % 2 == 0) {
                biggestEvenNumber = storageForFibonacciNumbers.get(i);
                break;
            }
        }
        return biggestEvenNumber;
    }
}
