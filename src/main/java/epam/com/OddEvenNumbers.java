/**
 * This package includes two classes.
 * Imported Vector collections.
 * <p>
 */
package epam.com;
import java.util.Vector;

/**
 * OddEvenNumber class writes odd or even numbers in
 * vectors, print them and gets sum of odd or even numbers.
 * @author Yura Tyschuk
 * @version 1.0
 */
public final class OddEvenNumbers {

    private final int startOfInterval;
    private final int endOfInterval;
    private Vector<Integer> oddNumbers;
    private Vector<Integer> evenNumbers;

    /**
     * @param startOfInterval gives starting number in sequence
     * @param endOfInterval gives final number in sequence
     */
    public OddEvenNumbers(final int startOfInterval, final int endOfInterval) {
        this.startOfInterval = startOfInterval;
        this.endOfInterval = endOfInterval;
        oddNumbers = new Vector<Integer>();
        evenNumbers = new Vector<Integer>();
    }


    public int getStartOfInterval() {
        return startOfInterval;
    }

    public int getEndOfInterval() {
        return endOfInterval;
    }

    public Vector<Integer> getOddNumbers() {
        return oddNumbers;
    }

    public Vector<Integer> getEvenNumbers() {
        return evenNumbers;
    }


    /**
     * This function adds odd numbers to vector oddNumbers.
     */
    public void writeOddNumbersToVector() {
        for (int i = startOfInterval; i <= endOfInterval; i += 2) {
                oddNumbers.add(i);
        }
    }

    /**
     * This function adds even numbers to vector evenNumbers.
     */
    public void writeEvenNumbersToVectorFromEnd() {
        for (int i = endOfInterval; i >= startOfInterval; i -= 2) {
                evenNumbers.add(i);
        }
    }

    /**
     * This function prints vector of odd numbers.
     */
    public void printVectorOfOddNumbers() {
        System.out.println(oddNumbers);
    }

    /**
     * This function prints vector of even numbers.
     */
    public void printVectorOfEvenNumbersFromEnd() {
        System.out.println(evenNumbers);
    }

    /**
     * This function gets sum of odd numbers.
     * @return int sum of odd numbers
     */
    public int getSumOfOddNumbers() {
        int sumOfOddNumbers = 0;
        for (int i = 0; i < oddNumbers.size(); i++) {
            sumOfOddNumbers += oddNumbers.get(i);
        }

        return sumOfOddNumbers;
    }

    /**
     * This function gets sum of even numbers.
     * @return int sum of even numbers
     */
    public int getSumOfEvenNumbers() {
        int sumOfEvenNumbers = 0;

        for (int i  = 0; i < evenNumbers.size(); i++) {
            sumOfEvenNumbers += evenNumbers.get(i);
        }

        return sumOfEvenNumbers;
    }

}
