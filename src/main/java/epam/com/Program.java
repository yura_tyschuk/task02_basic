package epam.com;

/**
 * The main method where  FibonacciNumbers and OddEvenNumbers are used.
 *
 */
public class Program {

    /**
     * This main function shows all functional of two classes.
     * @param args not used
     */
    public static void main(String[] args) {
        final int sizeOfSet = 20;
        FibonacciNumbers fibonacciNumbers = new FibonacciNumbers(sizeOfSet);
        fibonacciNumbers.calculateFibonacciNumbers();
        System.out.println("Fibonacci numbers: ");
        fibonacciNumbers.printFibonacciNumbers();
        System.out.println("The percentage of even numbers: "
                + fibonacciNumbers.getPercentageOfEvenNumbers() + "%");

        System.out.println("The percentage of odd numbers: "
                + fibonacciNumbers.getPercentageOfOddNumbers() + "%");

        System.out.println("The biggest odd number F1: "
                + fibonacciNumbers.getBiggestOddNumber());

        System.out.println("The biggest even number F2: "
                + fibonacciNumbers.getBiggestEvenNumber());


        final int startOfInterval = 1;
        final int endOfInterval = 20;
        OddEvenNumbers oddEvenNumbers = new OddEvenNumbers(startOfInterval, endOfInterval);
        oddEvenNumbers.writeEvenNumbersToVectorFromEnd();
        oddEvenNumbers.writeOddNumbersToVector();
        System.out.println("\nOdd numbers from start: ");
        oddEvenNumbers.printVectorOfOddNumbers();
        System.out.println("\nEven number from end: ");
        oddEvenNumbers.printVectorOfEvenNumbersFromEnd();
        System.out.println("\n Sum of even numbers: "
            + oddEvenNumbers.getSumOfEvenNumbers());

        System.out.println("\n Sum of odd numbers: "
            + oddEvenNumbers.getSumOfOddNumbers());

    }


}
